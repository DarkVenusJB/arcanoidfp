using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float speed;

    private Rigidbody2D rb;

   
    void Start()
    {
       rb = GetComponent<Rigidbody2D>(); 
    }

   
    void Update()
    {
        float horizontal = Input.GetAxis("Horizontal");

        Vector3 moveDirection = new Vector3(horizontal, 0f, 0f);
        rb.velocity = moveDirection*speed;
    }
}
